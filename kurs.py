# подключение библиотек
import time
import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def post_method():
    # ссылка куда будет отправляться post запрос
    url = "http://localhost/api/checkAuth"
    # ввод логина
    login = input("Введите логин:")
    # засекаем время
    search_time = time.time()
    # открываем словарь для перебора и начинаем сам перебор
    with open("rockyou-70.txt", "r") as file:
        for line in file.readlines():
            line = line.strip()
            # вставляем пароль из словаря
            data = {"username":login,"password":line}
            # делаем запрос
            res = requests.post(url, data=data)
            # если успешно, то пишем верный пароль и завершаем перебор
            if res.text == "{\"status\":\"success\"}":
                print("\nlogin: " + login +"\npassword: " + line)
                break
            # если не успешно, то пишем неверный пароль для отслеживания работы
            # print(line + " не подходит")
    # вывод затраченого времени
    print("\nВремя поиска: ", time.time() - search_time,  "\n")

def selenium_method():
    # готовим настройки для хрома, чтобы она работал в фоне
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    # запускаем веб драйвер
    driver = webdriver.Chrome(options=options, 
                executable_path=r"/home/oleg/allin/programming/kursach/chromedriver")
    # переходим на страницу http://localhost/
    driver.get("http://localhost/")
    # ищем на странице поля логина, пароля и индикатора, что мы вошли
    login = driver.find_element_by_id("username")
    password = driver.find_element_by_id("password")
    check = driver.find_element_by_id("indicator")
    # вставляем логин
    login.clear()
    log = input("Введите логин:")
    login.send_keys(log)
    # засекаем время
    search_time = time.time()
    # открываем словарь для перебора и начинаем сам перебор
    with open("rockyou-70.txt", "r") as file:
        for line in file.readlines():
            line = line.strip()
            # вставляем пароль из словаря
            password.clear()
            password.send_keys(line)
            # нажимаем войти
            driver.find_element_by_id("buttonCheck").click()
            # если успешно, то пишем верный пароль и завершаем перебор
            if check.text == "SUCCESS":
                print("\nlogin: " + log +"\npassword: " + line)
                break
            # если не успешно, то пишем неверный пароль для отслеживания работы
            # print(line + " не подходит")
    # вывод затраченого времени
    print("\nВремя поиска: ", time.time() - search_time, "\n")
    # скриншот для проверки
    driver.save_screenshot("test.png")
    # закрытие веб драйвера
    driver.close()

if __name__ == "__main__":
    while(True):
        method = int(input("Выберите способ подбора:\n1. Через Selenium\n2. Через POST запросы\n3. Выход\n>> "))
        if method == 1:
            selenium_method()
        elif method == 2:
            post_method()
        elif method == 3:
            exit()