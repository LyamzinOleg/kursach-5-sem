package main

import (
	"github.com/labstack/echo"
	"net/http"
	"os"
	"path/filepath"
)

func main() {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	var executionPath = filepath.Dir(ex)

	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.File(executionPath+"/index.html")
	})


	e.POST("/api/checkAuth", func (c echo.Context) error {
		var username = c.FormValue("username")
		var password = c.FormValue("password")

		if username == "test1" && password == "password"{
			return c.JSON(http.StatusOK, map[string]string{
				"status": "success",
			})
		}

		if username == "test2" && password == "chicken"{
        	return c.JSON(http.StatusOK, map[string]string{
        		"status": "success",
        	})
        }

		if username == "test3" && password == "ichigih13"{
        	return c.JSON(http.StatusOK, map[string]string{
        		"status": "success",
        	})
        }

        if username == "test4" && password == "BetQLT7qK2ev"{
        	return c.JSON(http.StatusOK, map[string]string{
        		"status": "success",
        	})
        }

		return c.JSON(http.StatusOK, map[string]string{
			"status": "error",
		})
	})

	e.Logger.Fatal(e.Start(":80"))
}